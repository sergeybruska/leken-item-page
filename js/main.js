$(".dropdown-menu li span").click(function(){
    var selText = $(this).text();
    $(this).parents('.btn-group').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
});
$(document).ready(function(){
    $("#star1").click(function(){
        $("#highrate").removeClass("form-hidden");
        $("#highrate").addClass("form-show");
        $("#lowrate").addClass("form-hidden");
        $("#lowrate").removeClass("form-show");
    });
    $("#star2").click(function(){
        $("#highrate").removeClass("form-hidden");
        $("#highrate").addClass("form-show");
        $("#lowrate").addClass("form-hidden");
        $("#lowrate").removeClass("form-show");
    });
    $("#star3").click(function(){
        $("#highrate").removeClass("form-show");
        $("#highrate").addClass("form-hidden");
        $("#lowrate").addClass("form-show");
        $("#lowrate").removeClass("form-hidden");
    });
    $("#star4").click(function(){
        $("#highrate").removeClass("form-show");
        $("#highrate").addClass("form-hidden");
        $("#lowrate").addClass("form-show");
        $("#lowrate").removeClass("form-hidden");
    });
    $("#star5").click(function(){
        $("#highrate").removeClass("form-show");
        $("#highrate").addClass("form-hidden");
        $("#lowrate").addClass("form-show");
        $("#lowrate").removeClass("form-hidden");
    });
});
$(".dropdown-menu .form-check label").click(function(){
    var selText = $(this).text();
    $(this).parents('.btn-group').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
    $(this).parents('.f-sort').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
    $(this).parents('.f-color').find('.dropdown-toggle').html(selText+' <span class="caret"></span>' + '...');
    $(this).parents('.f-type').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
    $("#filter-clear").removeClass("filter-hide");
    $("#filter-clear").addClass("filter-show");
    $('.keep-open').on('click', '.dropdown-menu > div', function(e) {
        e.stopPropagation();
    });
});
$('a[data-target="closedrop"]').on('click', function () {
    $('.dropdown').removeClass("show");
    $('.keep-open .dropdown-menu').collapse('hide');
    $a = $($(this).attr('href'));  
    return false;
});
$("#filter-clear").click(function(){
    $('.dropdown').removeClass("show");
    $("#filter-clear").removeClass("filter-show");
    $("#filter-clear").addClass("filter-hide");
    $(".f-sort .dropdown-toggle").replaceWith(function(){
        return ' <a href="" class="dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="caret">Сортировать</span></a> ';
    });
    $(".f-flower .dropdown-toggle").replaceWith(function(){
        return ' <a href="" class="dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="caret">Цветы</span></a> ';
    });
    $(".f-color .dropdown-toggle").replaceWith(function(){
        return ' <a href="" class="dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="caret">Цвет</span></a> ';
    });
    $(".f-type .dropdown-toggle").replaceWith(function(){
        return ' <a href="" class="dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="caret">Тип букета</span></a> ';
    });
    $("input:radio").prop("checked", false);
    $("input:checkbox").prop("checked", false);
    $('.keep-open .dropdown-menu').collapse('hide');
    $a = $($(this).attr('href'));  
    return false;
});

/* Mobile filter */
$("#filter-mobile-clear").click(function(){
    $("#filter-mobile").removeClass("catalog-filter--mobile");
    $("#filter-mobile").addClass("catalog-filter--mobile-off");
    $("input:radio").prop("checked", false);
    $("input:checkbox").prop("checked", false);
});
$(".mobile-fil-btn").click(function(){
    $("#filter-mobile").removeClass("catalog-filter--mobile");
    $("#filter-mobile").addClass("catalog-filter--mobile-off");
});
$(".btn-close").click(function(){
    $("#filter-mobile").removeClass("catalog-filter--mobile");
    $("#filter-mobile").addClass("catalog-filter--mobile-off");
});

$("#filter-mob-btn").click(function(){
    $("#filter-mobile").removeClass("catalog-filter--mobile-off");
    $("#filter-mobile").addClass("catalog-filter--mobile");
});


/* Menu */

$(document).ready(function(){
    $("#popup-btn").click(function(){
        $("#popup-form").removeClass("popup-menu");
        $("#popup-form").addClass("popup-menu-on");
    });
    $(".close-btn").click(function(){
        $("#popup-form").removeClass("popup-menu-on");
        $("#popup-form").addClass("popup-menu");
    });
    $("#picture-add").click(function(){
        $("#picture-form").removeClass("form-hidden");
        $("#picture-form").addClass("form-show");
        $("#picture-add").addClass("form-hidden");
    });
    $("#comment-add").click(function(){
        $("#comment-form").removeClass("form-hidden");
        $("#comment-form").addClass("form-show");
        $("#comment-add").addClass("form-hidden");
    });
    $("#flowers-mob").click(function(){
        $("#flowers-other").removeClass("flowers-hidden");
        $("#flowers-other").addClass("flowers-show");
        $("#flowers-mob").addClass("flowers-hidden");
    });
    $("#logSwitchMail").click(function(){
        $('.keep-open').on('click', '.dropdown-menu > div', function(e) {
            e.stopPropagation();
        });
        $("#loginEmail").removeClass("top-menu-hide");
        $("#loginEmail").addClass("top-menu-block");
        $("#loginPhone").addClass("top-menu-hide");
    });
    $("#logSwitchPhone").click(function(){
        $('.keep-open').on('click', '.dropdown-menu > div', function(e) {
            e.stopPropagation();
        });
        $("#loginPhone").removeClass("top-menu-hide");
        $("#loginPhone").addClass("top-menu-block");
        $("#loginEmail").addClass("top-menu-hide");
    });
});

$("#block-box input[type='checkbox']").click(function(){
    $("#block-box input[type='checkbox']").not(this).each(function(){
      this.checked = false; 
      $(".blocks").addClass("form-show");
      $("#checkoutForm1").addClass("form-hidden"); 
    });
    if (this.checked) {
        $(".blocks").addClass("form-hidden").removeClass("form-show");
        $("#checkoutForm1").addClass("form-show").removeClass("form-hidden"); 
        $("#checkoutForm2").addClass("form-hidden").removeClass("form-show"); 
        } else {
            $(".blocks").removeClass("form-hidden");
            $(".blocks").addClass("form-show"); 
            $("#checkoutForm1").addClass("form-hidden").removeClass("form-show"); 
            $("#checkoutForm2").addClass("form-show").removeClass("form-hidden");
        }
});

$("#block-box2 input[type='checkbox']").click(function(){
    $("#block-box2 input[type='checkbox']").not(this).each(function(){
      this.checked = false; 
    });
    if (this.checked) {
        $('#checkoutForm3').find("input[type=text]").each(function(ev)
            {
                if(!$(this).val()) { 
                $(this).attr("placeholder", "Позвоним получателю и узнаем адрес доставки");
                $("#checkoutForm3 > input").prop('disabled', true);
             }
    });
        } else {
            $("#checkoutForm3 > input").attr("placeholder", "Улица, дом, квартира/офис");
            $("#checkoutForm3 > input").prop('disabled', false);
        }
});

$('.scroll').click(function() {
    var sectionTo = $(this).attr('href');
    $('html, body').animate({
      scrollTop: $(sectionTo).offset().top
    }, 200);
});

$(function () {
    $('[data-toggle="tooltip"]').tooltip();
  });

$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    navText:false,
    dots:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
});
$('.owl-calendar').owlCarousel({
    loop:false,
    margin:10,
    nav:true,
    dots:false,
    navText:false,
    responsive:{
        0:{
            items:4
        },
        600:{
            items:7
        },
        1000:{
            items:7
        }
    }
});


$(document).ready(function(){
    $(".dropdown-filter-menu").dropdown();
}); 


function myFunction() {
    document.getElementById("drop").classList.toggle("show");
  }
  
  // Close the dropdown menu if the user clicks outside of it
  window.onclick = function(event) {
    if (!event.target.matches('.dropbtn')) {
      var dropdowns = document.getElementsByClassName("dropdown-content");
      var i;
      for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.classList.contains('show')) {
          openDropdown.classList.remove('show');
        }
      }
    }
  };


 // calendar 
 $(document).ready(function() {
    $("#datepicker").click(function() {
        var test = $(this).val();
        $("div.checkout-calendar").toggle(); 
    });
});
$(".checkout-calendar .owl-calendar .item span span.day").click(function(){
    var dayText = $(this).text();
    $(this).parents('.col-sm-10').find('.dropdown-calendar').html(dayText+' <span class="caret"></span>');
    $("div.checkout-calendar").toggle();
});

$(".day-act").click(function(e) {
    e.preventDefault();
    $(".day-act").not(this).removeClass('active');
    $(this).toggleClass('active');
});


